// here are your global variables
let playerTotalScore = 0;
let currentPlayer = 0;

const loadGame = function() {

    // 1. get saved game data from the local storage
    // ----------
    
    // 1a. get score from localStorage
    let score = localStorage.getItem("p1Total");
    // 1b. convert the score to an integer
    playerTotalScore = parseInt(score);

    // 1c. get the current player from localStorage and convert to int
    currentPlayer = localStorage.getItem("currentPlayer");
    currentPlayer = parseInt(currentPlayer);



    // @TODO: get saved roll value
    



    // 2. update the user interface
    // -------------------

    // update the total score UI
    document.querySelector("#total-0").innerText = playerTotalScore;

    // @TODO: update the roll value
    // @TODO: update the dice image

    // update the current player UI
    if (currentPlayer === 1) {
        document.querySelector("div.player-0-panel")
            .classList.remove("active");
        document.querySelector("div.player-1-panel")
            .classList.add("active");
    }
    else if (currentPlayer === 2) {
        document.querySelector("div.player-0-panel")
        .classList.add("active");
    document.querySelector("div.player-1-panel")
        .classList.remove("active");
    }
}

const rollDice = function() {

    // 1. generate random value
    let diceValue = Math.floor(Math.random() * 6 ) + 1;
    console.log("You rolled: " + diceValue);

    // 2. update total score
    playerTotalScore = playerTotalScore + diceValue;
    console.log("Total score: " + playerTotalScore);
    
    // 3. update dice image, roll label, total score label

    let imageName = "images/dice-" + diceValue + ".png"
    document.querySelector("img.dice").src = imageName;

    document.querySelector("#score-0").innerText = diceValue;
    document.querySelector("#total-0").innerText = playerTotalScore;

    // 4. if they roll 1, reset
    if (diceValue === 1) {
        playerTotalScore = 0;
        console.log("You rolled a 1! Resetting score: " + playerTotalScore);

        document.querySelector("#total-0").innerText = 0;
    }

    // 5. if they get 100, win
    if (playerTotalScore === 100) {
        console.log("YOU WIN!");
    }

    // 6. AUTO SAVE CURRENT GAME
    localStorage.setItem("currentPlayer", currentPlayer);
    localStorage.setItem("p1Total", playerTotalScore);


}


const changePlayer = function() {
    console.log("CURRENT PLAYER IS:" + currentPlayer);
    console.log("CHANGING PLAYERS!");

    if (currentPlayer === 1) {
        currentPlayer = 2;

        // UI LOGIC
        document.querySelector("div.player-0-panel")
            .classList.remove("active");
        document.querySelector("div.player-1-panel")
            .classList.add("active");
    }
    else if (currentPlayer === 2) {
        currentPlayer = 1
        document.querySelector("div.player-0-panel")
        .classList.add("active");
    document.querySelector("div.player-1-panel")
        .classList.remove("active");

    }
    console.log(currentPlayer);
    
    //  AUTOSAVE THE CURRENT PLAYER
    localStorage.setItem("currentPlayer", currentPlayer);

}

// click handler
document.querySelector(".btn-roll").addEventListener("click", rollDice);
document.querySelector(".btn-hold").addEventListener("click", changePlayer);


// built in Javascript function to detect when window reloads
window.onload = function() {

    console.log("window has loaded");
    
    // function to load the saved data from localStorage
    loadGame();
    
}